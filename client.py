from socket import *
import os

def cls():
    os.system("cls" if os.name == "nt" else "clear")

HOST = "127.0.0.1"  # Konfigurasi HOST
PORT = 6969 # Konfigurasi PPORT

s = socket(AF_INET, SOCK_STREAM)

s.connect((HOST, PORT))

message = s.recv(2048)
print (message.decode())

while True :
    try:
        jawaban = input(":> ")
        if message:
            answer2 = input("Sudah benar? (Y/N):")

            if answer2 == "Y" or answer2 == "y":
                s.send(jawaban.encode())
            else:
                print ("Silakan koreksi!")
                continue
        elif jawaban == "keluar":
            s.close()
            exit(0)
        else:
            s.send(jawaban.encode())
        cls()
        message = s.recv(2048)
        if(";" in message.decode()):
            soal = message.decode().split(";")
            print(soal[0])
            print('A.',soal[1])
            print('B.',soal[2])
            print('C.',soal[3])
            print('D.',soal[4])
        else:
            print (message.decode())


    except:
        s.close()
        exit(0)