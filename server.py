from socket import *
import threading
import time
import random
import csv

class Server():

    def listenToClient(self, client, addr):      
        while True:
            client.send("Selamat Datang! \nMasukkan Username".encode())
            username = client.recv(2048).decode()
            client.send("Masukkan kata sandi: ".encode())
            katasandi = client.recv(2048).decode()

            print ("Username: ", username)
            print ("katasandi: ", katasandi)

            usernamefile = open("username.csv", "r", encoding="utf-8")
            readUser = csv.reader(usernamefile, delimiter=',')
            usernames = []
            for row in readUser:
                usrnm = {'user': row[0], 'password': row[1]}
                usernames.append(usrnm)
            for i in range(len(usernames)):
                if (username.lower() == usernames[i]['user'] and katasandi.lower() == (usernames[i]['password'])):
                    if (usernames[i]['user'] == 'admin'):
                        client.send("Selamat datang Admin!\n".encode())
                        def menu():
                            client.send("------- MENU -------\n".encode())
                            client.send("1 : Tambah User\n".encode())
                            client.send("2 : Tambah Soal\n".encode())
                            client.send("3 : Keluar\n".encode())
                            
                            menupil = client.recv(2048).decode()
                            if menupil == '1':
                                tambahUser()
                            elif menupil == '2':
                                tambahSoal()
                            elif menupil == '3':
                                pass
                            else:
                                menu()
                        def tambahUser():
                            client.send("Input username baru".encode())
                            users = client.recv(2048).decode()
                            client.send("Input password baru".encode())
                            pwds = client.recv(2048).decode()
                            addUser = open('username.csv', 'a+')
                            addUser.write(users)
                            addUser.write(',')
                            addUser.write(pwds)
                            addUser.write('\n')
                            addUser.close()
                            client.send("Tambah user berhasil!\n".encode())
                            menu()

                        def tambahSoal():
                            client.send("Format penulisan soal : soal,jawaban A,jawaban B,jawaban C,jawaban D,Jawaban Soal\n".encode())
                            client.send("Contoh penulisan soal : Kuda adalah,Hewan,Amfibi,Bintang,Meteor,a\n".encode())
                            client.send("Tulis soal baru\n".encode())
                            soal = client.recv(2048).decode()
                            addSoal = open('pertanyaan.csv', 'a+')
                            addSoal.write(soal)
                            addSoal.write('\n')
                            addSoal.close()
                            client.send("Tambah soal berhasil!\n".encode())
                            menu()

                        menu()
                        
                    else: # Else admin
                        client.send("Autentification berhasil.\n".encode())
                        file2 = open("kehadiran.txt", "a+")
                        file2.write(username)
                        file2.write(": hadir\n")
                        file2.close()

                        localtime1 = time.localtime(time.time())[4]
                        print ("Time->", time.localtime(time.time())[3], ":", time.localtime(time.time())[4])

                        # Open pertanyaan
                        questionFile = open("pertanyaan.csv", "r", encoding="utf-8")
                        reader = csv.reader(questionFile, delimiter=',')
                        questions = []
                        for row in reader:
                            quest = {'soal': row[0], 'opsi': {'A': row[1], 'B': row[2], 'C': row[3], 'D': row[4]}, 'jawaban': row[5] }
                            questions.append(quest)
                        random.shuffle(questions)

                        # Memberi nilai jika jawaban benar
                        nilai = 0

                        for number in range(len(questions)):
                            data = "%s;%s;%s;%s;%s" % (
                            questions[number]['soal'], questions[number]['opsi']['A'], questions[number]['opsi']['B'],
                            questions[number]['opsi']['C'], questions[number]['opsi']['D'])
                            client.send(data.encode())
                            jmlhsoal = len(questions)
                            htgsoal = 100 / jmlhsoal
                            answer = client.recv(2048).decode()
                            if answer.lower() == questions[number]['jawaban']:
                                nilai += htgsoal

                        localtime2 = time.localtime(time.time())[4]
                        timestamp = localtime2 - localtime1
                        if timestamp > 30:
                            print("Waktumu habis\n")
                            client.close()
                        # Display total score
                        bonus = 10 / (timestamp + 1)
                        totalscore = nilai + bonus
                        print(username, "score", "%.2f" % (nilai), "bonus", str(bonus), "total Score:",
                              "%.2f" % (totalscore))
                        # Write Score to File
                        fileScore = open("scores.txt", "a+")
                        fileScore.write(username)
                        fileScore.write(" : Bonus ")
                        fileScore.write(str(bonus))
                        fileScore.write(", Nilai : ")
                        fileScore.write("%.2f" % (nilai))
                        fileScore.write(", Total Score : ")
                        fileScore.write("%.2f" % (totalscore))
                        fileScore.write("\n")
                        fileScore.close()
                        scoreclient = "Total score anda : " + "%.2f" % (totalscore) + "\n"
                        client.send(scoreclient.encode())

            usernamefile.close()


    def __init__(self, PORT, HOST):
        try:
            serverSocket = socket(AF_INET, SOCK_STREAM)
        except:
            print ("Socket tidak terbuat")
            exit(1)
            # Sockets are the endpoints of a bidirectional communications channel.
        print ("Socket telah dibuat")
        try:
            serverSocket.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
        except:
            print ("Socket tidak dapat digunakan")
            exit(1)
        print ("Socket sedang digunakan")
        try:
            serverSocket.bind((HOST, PORT))
        except:
            print ("Bind gagal")
            exit(1)
        print ("Bind berhasil")
        try:
            serverSocket.listen(45)
        except:
            print ("Server tidak dapat mendengarkan!")
            exit(1)
        print ("Server siap menerima")

        while True:
            connectionSocket, addr = serverSocket.accept()

            threading.Thread(target=self.listenToClient, args=(connectionSocket, addr)).start()

if __name__ == "__main__":
    HOST = "127.0.0.1" # Konfigurasi HOST
    PORT = 6969        # Konfigurasi PORT
    Server(PORT, HOST)

