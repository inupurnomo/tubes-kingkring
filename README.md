# Tubes-Kingkring

# Aplikasi Quiz menggunakan Socket
## Penjelasan
Aplikasi Quiz ini kami buat untuk melakukan quiz saat perkuliahan atau untuk bermain games. Soal quiz akan selalu diacak kemungkinan antar client akan mendapatkan soal yang berbeda, dan menampilkan nilai user saat user sudah selesai dan nilainya pun di simpan di server.

## Anggota
1. Ilham Ibnu Purnomo (1301164089)
2. Adisca Naufal R (1301164358)
3. Maulana Azizwara (1301160210)

## Fitur 
1.  Menampilkan soal secara acak
2.  Autentifikasi user
3.  Dapat menampilkan kehadiran
4.  Dapat melihat nilai
5.  Dapat menyimpan data kehadiran
6.  Dapat menyimpan data nilai
7.  Dapat menambah user
8.  Dapat menambah soal

## Detail Server
+ HOST : 127.0.0.1 (local)
+ PORT : 6969

## Requirements
+ Python 3
+ Socket

